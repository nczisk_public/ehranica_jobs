import gnupg

gpg = gnupg.GPG('C:/Program Files (x86)/GnuPG/bin/gpg.exe')

public_keys = gpg.list_keys()
for key in public_keys:
    ascii_armored_public_keys = gpg.export_keys(key['keyid'])
    with open(key['keyid']+'.pub','wb') as f:
        f.write(ascii_armored_public_keys.encode())


priv_keys = gpg.list_keys(True)
for key in priv_keys:
    ascii_armored_private_keys = gpg.export_keys(key['keyid'], True, passphrase='nbusr123')
    with open(key['keyid']+'.priv','wb') as f:
        f.write(ascii_armored_private_keys.encode())


