import gnupg
import os

this_dir, this_filename = os.path.split(__file__)

gpg = gnupg.GPG()

keyid = '989440B0903DEB5C'

with open( os.path.join(this_dir, keyid + '.priv'), 'r') as f:
    keydata = f.read()
    import_result = gpg.import_keys(keydata)
    print(import_result.count)
    print(import_result.fingerprints)
    gpg.trust_keys(import_result.fingerprints,'TRUST_ULTIMATE')




