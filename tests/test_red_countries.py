import requests_mock

from sftp_jobs_ehranica.jobs import JobRedCountries
from sftp_jobs_ehranica.telco import Operator, Uvz
from sftp_jobs_ehranica.utils import setup_log

red_countries = ['AFG', 'SLB', 'ALB', 'DZA', 'AND','AGO']

def test_logs(config):
    setup_log(config)


def test_folders(config):
    # check operators folders
    Operator.initialize_all_operators(config, check_and_create_paths=True)
    # check keboola folders
    Uvz(config, check_and_create_paths=True)



def test_prepare_red_countries(config):
    uvz = Uvz(config, check_and_create_paths=True)
    uvz.create_red_countries(red_countries)



def test_red_countries(config):
    with requests_mock.Mocker() as m:
        m.post( config['api']['red_countries_api'] , text='')
        job = JobRedCountries(config)
        rt = job._run_implementation()
        assert rt
        assert len(rt) == len(red_countries)
        for top in job.operators:
            assert top.red_countries_exists()

