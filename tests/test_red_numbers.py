import requests_mock
import os
from sftp_jobs_ehranica.jobs import JobRedNumbers
from sftp_jobs_ehranica.telco import Operator

red_numbers = ['+4219051232356', '+421222123456', '+4212323123456', '+421905123322']



def test_prepare_red_numbers(config):
    operators = Operator.initialize_all_operators(config, check_and_create_paths=True)
    recipent = config['gpg']['private_recipient']

    red_numbers_buffer = ''
    for x in red_numbers:
        red_numbers_buffer += x + '\n'

    for ope in operators:
        ope.create_red_numbers(red_numbers_buffer, recipent)



def test_red_numbers(config):
    with requests_mock.Mocker() as m:
        m.post( config['api']['red_numbers_api'] , text='')
        job = JobRedNumbers(config)
        rt = job._run_implementation()
        assert rt
        assert len(rt.keys()) == 4
        for file in rt:
            assert not os.path.exists(file)
            num_list = rt[file]
            assert len(num_list) == len(red_numbers)

