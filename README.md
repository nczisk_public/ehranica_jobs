# ehranica_jobs

## Required 

python 3.6, gpg

## Instalacia/upgrade

Instalacia venv (ak nie je)

`pip install virtualenv`

Vytvorenie virtualenv

`virtualenv venv`

Aktivacia virtualenv

`source venv/bin/activate`

Instalacia sw (priklad pre verziu 0.0.7)

`pip install -e git+https://gitlab.com/nczisk_public/ehranica_jobs.git@0.0.7#egg=ehranica_jobs`

Pre upgrade staci pouzit inu verziu  napr.
`pip install -e git+https://gitlab.com/nczisk_public/ehranica_jobs.git@0.0.8#egg=ehranica_jobs --upgrade`


## Konfiguracia

Template konfiguracie sa nachadza. Pouzijeme ak instalujeme prvy krat.
 
`venv/src/ehranica-jobs/sftp_jobs/config_templates/config.json`

Pri prvej instalacii skopirujeme do konfig adresara

`mkdir ~/sftp_jobs
cp  ./venv/src/ehranica-jobs/sftp_jobs/config_templates/config.json ~/sftp_jobs
`

Nasledne editovat tuto konfiguraciu


## Beh

`./sftp_jobs_runner_ehranica

usage: sftp_jobs_runner [-h] --config CONFIG --job
                        [{all,red_countries,red_numbers}]`
                        
--config - plna cesta na konfiguracny subor
 
--job - aky job chceme spustit, vhodne ak chceme spustat samostane kazdu funkcionalitu. 

Ak chceme vsetko tak all (spusti sa v poradi red_countries,red_numbers )
* red_countries - zoberie zoznam red_countries od uvz a nakopiruje pre operatorov
* red_numbers - preberie data od operatorv a posunie cez API dalej


Logy behu aplikacie su generovane v suboroch s rotaciou podla konfigu
                        