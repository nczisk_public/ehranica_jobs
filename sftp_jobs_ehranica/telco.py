import logging
import os
import time
import gnupg
import requests

from sftp_jobs_ehranica.utils import LOG_APP, perf, check_path

LOG = logging.getLogger(LOG_APP)



class Api:

    def __init__(self, conf):
        self.conf = conf
        self.bearer = conf['api']['bearer']
        self.red_countries_api = conf['api']['red_countries_api']
        self.red_numbers_api = conf['api']['red_numbers_api']

    @staticmethod
    def red_countries_param_prepare(red_countries_list):
        rt = []
        for ccode in red_countries_list:
            rt.append(
                {
                    'vISO3CountryCode': ccode
                }
            )
        return rt

    @perf
    def red_countries_send(self, red_countries_list):
        params =  self.red_countries_param_prepare(red_countries_list)
        headerz = {}
        if self.bearer:
            headerz['Authorization'] =  "Bearer {}".format(self.bearer)
        LOG.debug('Headers {}'.format(headerz))
        resp = requests.post(self.red_countries_api, json=params, headers=headerz)
        if resp.status_code != 200:
            LOG.error('{}{}'.format(resp.status_code, resp.reason))
            return None
        return True


    @staticmethod
    def red_numbers_param_prepare(operator, red_numbers_list):
        rt = {
            'vTelcoOperatorUUID': operator,
            'vPhoneNumber': red_numbers_list
        }
        return rt

    @perf
    def red_numbers_send(self, operator, red_numbers_list):
        params =  self.red_numbers_param_prepare( operator, red_numbers_list)
        headerz = {}
        if self.bearer:
            headerz['Authorization'] =  "Bearer {}".format(self.bearer)
        LOG.debug('Headers {}'.format(headerz))
        resp = requests.post(self.red_numbers_api, json=params, headers=headerz)
        if resp.status_code != 200:
            LOG.error('{}{}'.format(resp.status_code, resp.reason))
            return None
        return True



class Operator:

    def __init__(self, conf, op_name, check_and_create_paths = False):
        self.conf = conf
        self.op_name = op_name
        self.home_path = conf['operators'][op_name]['home_path']
        check_path(self.home_path,check_and_create_paths )
        self.red_path = os.path.join(self.home_path,'red')
        check_path(self.red_path, check_and_create_paths)
        self.gpg_recipient = conf['operators'][op_name]['gpg_recipient']
        if not conf['gpg']['gpgbinary'] or len(conf['gpg']['gpgbinary']) == 0:
            self.gpg = gnupg.GPG()
        else:
            self.gpg = gnupg.GPG(gpgbinary=conf['gpg']['gpgbinary'])

    @staticmethod
    def initialize_all_operators(conf, check_and_create_paths = False):
        rt = []
        for x in conf['operators']:
            rt.append(Operator(conf,x, check_and_create_paths))
        return rt


    @perf
    def create_red_countries(self, countries_list):
        fname = os.path.join(self.red_path,'countries.csv')
        buff = ''
        for x in countries_list:
            buff += x + '\n'
        with open( fname, 'w') as fp:
            fp.write(buff)
        LOG.debug('Created {}'.format(fname))

    def red_countries_exists(self):
        return os.path.exists(os.path.join(self.red_path,'countries.csv'))


    @perf
    def get_all_red_numbers(self):
        redfiles = [os.path.join(self.red_path, f) for f in os.listdir(self.red_path) if os.path.isfile(os.path.join(self.red_path, f))]
        rt = {}
        for f in redfiles:
            bname = os.path.basename(f)
            if not bname.lower().startswith('numbers_'):
                continue
            try:
                if f.lower().endswith('.pgp') or f.lower().endswith('.gpg'):
                    decrypted, data = self.decrypt(f)
                    if decrypted:
                        if not data or len(data) == 0:
                            rt[f] = []
                        else:
                            rt[f] = data.splitlines()
                        LOG.debug('Loaded {}'.format(f))
                    else:
                        LOG.error('Not possible to decrypt {}'.format(f))
                elif f.lower().endswith('csv'):
                    with open(f, "r") as fp:
                        rt[f] = fp.read().splitlines()
                        LOG.warning('Loaded unencrypted {}'.format(f))
            except Exception as e:
                LOG.error(e)
        return rt

    @perf
    def encrypt(self, buff, recipient):
        return self.gpg.encrypt(buff, [recipient])

    @perf
    def decrypt(self, file_name):
        with open(file_name, "rb") as f:
            status = self.gpg.decrypt_file(f, passphrase=self.conf['gpg']['passphrase'])
            if status.ok:
                return True, status.data.decode()
        return False, None

    @perf
    def create_red_numbers(self, red_numbers_buffer, reciptient):
        # function just for test purposes
        t = time.localtime()
        timestamp = time.strftime('%Y%m%d%H%M%S', t)
        fname = os.path.join(self.red_path,'numbers_'+timestamp+'.csv')
        if self.gpg_recipient and len(str(self.gpg_recipient).strip()) > 0:
            fname = fname + '.pgp'
            result = self.encrypt(red_numbers_buffer, reciptient)
            if result.ok:
                with open( fname, 'wb') as fp:
                    fp.write(result.data)
                LOG.debug('Created red_numbers {} in {}'.format(fname, self.op_name))
                return fname
            else:
                LOG.error('Failed to encrypt to {}'.format(fname))
                return None
        else:
            with open(fname, 'w') as fp:
                fp.write(red_numbers_buffer)
            LOG.warning('Created unencrypted red_numbers {}'.format(fname))
            LOG.debug('Created red_numbers {} in {}'.format(fname, self.op_name))
            return fname


class Uvz:

    def __init__(self, conf, check_and_create_paths = False):
        self.conf = conf
        self.op_name = 'uvz'
        self.home_path = conf[self.op_name]['home_path']
        check_path(self.home_path, check_and_create_paths)
        self.red_path = os.path.join(self.home_path,'red')
        check_path(self.red_path, check_and_create_paths)

    @perf
    def get_red_countries(self):
        rt = []
        fname = os.path.join(self.red_path,'countries.csv')
        with open(fname, "r") as fp:
            rt = fp.read().splitlines()
        return rt

    @perf
    def create_red_countries(self, countries_list):
        fname = os.path.join(self.red_path,'countries.csv')
        buff = ''
        for x in countries_list:
            buff += x + '\n'
        with open( fname, 'w') as fp:
            fp.write(buff)
        LOG.debug('Created {}'.format(fname))
