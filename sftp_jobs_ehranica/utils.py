import os
import logging
import time
from functools import wraps
from logging.handlers import RotatingFileHandler
from pathlib import Path

LOG_APP = 'app'
LOG_PERF = 'perf'
LOG_OTHERS = 'others'

LOG = logging.getLogger(LOG_APP)
PLOG = logging.getLogger(LOG_PERF)


def base_dir():
    basedir = os.path.join(str(Path.home()), '.sftp_jobs_ehranica')
    if not os.path.exists(basedir):
        os.makedirs(basedir)
    return basedir

def perf(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        t1 = time.time()
        rt = f(*args, **kwargs)
        PLOG.debug("{}() {:.4f} ms.".format(f.__name__,(time.time() - t1) * 1000.0))
        return rt
    return decorated


def setup_log(conf):
    if not os.path.exists(conf['log']['logdir']):
        os.makedirs(conf['log']['logdir'], exist_ok=True)

    # olog = logging.StreamHandler()
    olog = RotatingFileHandler(
        os.path.join(conf['log']['logdir'], conf['log']['logfileprefix'] + '.' + LOG_OTHERS + '.log'),
        maxBytes=conf['log']['logmaxbytes'], backupCount=conf['log']['logcount'])
    olog.setLevel(conf['log'][LOG_OTHERS])
    olog.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(filename)s.%(funcName)s %(message)s'))
    logroot = logging.getLogger()
    logroot.setLevel(conf['log'][LOG_OTHERS])
    logroot.addHandler(olog)

    # ch = logging.StreamHandler()
    ch = RotatingFileHandler(os.path.join(conf['log']['logdir'], conf['log']['logfileprefix'] + '.' + LOG_APP + '.log'),
                             maxBytes=conf['log']['logmaxbytes'], backupCount=conf['log']['logcount'])

    ch.setLevel(conf['log'][LOG_APP])
    ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(funcName)s - - %(message)s'))
    alog = logging.getLogger(LOG_APP)
    alog.setLevel(conf['log'][LOG_APP])
    alog.addHandler(ch)

    # perf = logging.StreamHandler()
    perf = RotatingFileHandler(
        os.path.join(conf['log']['logdir'], conf['log']['logfileprefix'] + '.' + LOG_PERF + '.log'),
        maxBytes=conf['log']['logmaxbytes'], backupCount=conf['log']['logcount'])

    perf.setLevel(conf['log'][LOG_PERF])
    perf.setFormatter(logging.Formatter('%(asctime)s - PERF - %(funcName)s - %(message)s'))
    plog = logging.getLogger(LOG_PERF)
    plog.setLevel(conf['log'][LOG_PERF])
    plog.addHandler(perf)


def check_path(p,check_and_create_paths = False):
    if not os.path.exists(p):
        if check_and_create_paths:
            LOG.warning('Path {} not exists - creating'.format(p))
            os.makedirs(p)
        else:
            LOG.error('Path {} not exists'.format(p))

