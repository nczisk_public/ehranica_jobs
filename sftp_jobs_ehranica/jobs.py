from abc import ABC, abstractmethod

from sftp_jobs_ehranica.telco import *

LOG = logging.getLogger(LOG_APP)


class Job(ABC):

    def __init__(self, conf):
        self.conf = conf

    @abstractmethod
    def _run_implementation(self):
        pass

    @perf
    def run(self):
        try:
            self._run_implementation()
        except Exception as e:
            LOG.error(e, exc_info=True)


class JobRedCountries(Job):

    def __init__(self, conf):
        super().__init__(conf)
        # initialize operators
        self.operators = Operator.initialize_all_operators(conf)
        # initialize uvz
        self.uvz = Uvz(conf)

    def _run_implementation(self):
        rt = []
        LOG.debug('Processing red_countries start')
        api = Api(self.conf)
        # get countries from uvz
        countries = self.uvz.get_red_countries()
        LOG.debug('red_countries data {}'.format(countries))
        for top in self.operators:
            top.create_red_countries(countries)
        # post countries to moje ezdravie backend
        try:
            api.red_countries_send(countries)
            LOG.info('Processed {} red_countries list'.format(len(countries)))
            rt = countries
        except Exception as e:
            LOG.error(e)
        LOG.debug('Processing red_countries finish')
        return rt



class JobRedNumbers(Job):

    def __init__(self, conf):
        super().__init__(conf)
        # initialize operators
        self.operators = Operator.initialize_all_operators(conf)
        # initialize uvz
        self.uvz = Uvz(conf)
        self.chunk_size = 500

    @staticmethod
    def divide_chunks(_arr, n):
        # looping till length l
        for i in range(0, len(_arr), n):
            yield _arr[i:i + n]


    def _run_implementation(self):
        rt = {}
        LOG.debug('Processing red_numbers start')
        api = Api(self.conf)
        for top in self.operators:
            LOG.debug('Processing red_numbers for {}'.format(top.op_name))
            red_list  = top.get_all_red_numbers()
            if not red_list:
                continue
            # substitute
            for file in red_list:
                errors = 0
                LOG.info('Processing file {} with {} entries'.format(file, len(red_list[file])))
                LOG.debug('Red_numbers file {} contains  {}'.format(file, red_list[file]))
                for chunk in  self.divide_chunks(red_list[file], self.chunk_size):
                    try:
                        if not api.red_numbers_send(top.op_name, chunk):
                            LOG.error('There is problem with chunk {}'.format(chunk))
                            errors += 1
                        else:
                            LOG.debug('Send {} numbers in chunk'.format(len(chunk)))
                    except Exception as e:
                        LOG.error(e)
                        errors += 1
                if errors == 0:
                    os.remove(file)
                    LOG.debug('Removing {}'.format(file))
                rt[file] = red_list[file]
        LOG.debug('Processing red_numbers finish')
        return rt