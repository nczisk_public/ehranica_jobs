import os
from setuptools import setup

setup(
    name='ehranica_jobs',
    version='0.0.2',
    packages=['sftp_jobs_ehranica'],
    url='https://gitlab.com/nczisk_public/ehranica_jobs',
    classifiers=['Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.6',
                 'Intended Audience :: Developers',
                 'Environment :: Console',
                 ],

    platforms=['Any'],
    install_requires=['requests', 'python-gnupg'],
    license='MIT License',
    author='rho',
    author_email='richard.holly@nczisk.sk',
    description='',
    include_package_data=True,
    package_data={
        'sftp_jobs_ehranica': ['config_templates/*'],
    },
    entry_points={
        'console_scripts':['sftp_jobs_runner_ehranica = sftp_jobs_ehranica.runner:main']
    }
)
